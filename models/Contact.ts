interface Contact {
  uid?: string;
  name: string;
  phoneNumber: string;
  imageUri: string;
  isFavourite: boolean;
  landlineNumber: string;
}

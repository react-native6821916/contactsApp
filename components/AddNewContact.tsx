import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {
  CameraOptions,
  ImageLibraryOptions,
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker';

import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../App';

import {openDatabase} from 'react-native-sqlite-storage';

var db = openDatabase({name: 'ContactDatabases.db'});

type AddContactProps = NativeStackScreenProps<
  RootStackParamList,
  'AddNewContact'
>;

export default function AddNewContact({route, navigation}: AddContactProps) {
  useEffect(() => {
    db.transaction(txn => {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='Contacts'",
        [],
        (tx: any, res: {rows: string | any[]}) => {
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS Contacts', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS  Contacts (uid INTEGER PRIMARY KEY, name VARCHAR(255) NOT NULL, phoneNumber VARCHAR(20) NOT NULL, imageUri VARCHAR(255) NOT NULL,isFavourite BOOLEAN NOT NULL, landlineNumber VARCHAR(20) NOT NULL)',
              [],
            );
          }
        },
      );
    });
  }, []);

  const emptyStarUri = require('../images/unfavIcon.png');
  const filledStarUri = require('../images/favIcon.png');
  const backIconUri = require('../images/backIcon.png');
  const emptyImageUri = 'https://shorturl.at/dhovO';

  const [starUri, setStarUri] = useState(emptyStarUri);
  const [selectedImage, setSelectedImage] = useState(emptyImageUri);
  const [name, setName] = useState('');
  const [isValidName, setIsValidName] = useState(true);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [landlineNumber, setLandlineNumber] = useState('');
  const [phoneNumberValid, setPhoneNumberValid] = useState(true);
  const [landlineNumberValid, setLandlineNumberValid] = useState(true);
  const [isFavourite, setIsFavourite] = useState(false);
  const [isFromFavourite, setIsFromFavourite] = useState(false);

  const contact: Contact = {
    name: name,
    phoneNumber: phoneNumber,
    imageUri: selectedImage,
    isFavourite: isFavourite,
    landlineNumber: landlineNumber,
  };

  useEffect(() => {
    if (route.params && route.params.contact && route.params.contact.uid) {
      setName(route.params.contact.name);
      setIsValidName(true);
      setPhoneNumber(route.params.contact.phoneNumber);
      setSelectedImage(route.params.contact.imageUri);
      setLandlineNumber(route.params.contact.landlineNumber);
      if (route.params.contact.isFavourite) changeUri(emptyStarUri);
      setPhoneNumberValid(true);
      setLandlineNumberValid(true);
      setIsFromFavourite(route.params.isFromFavourite);
    }
  }, [route.params.contact]);

  const changeUri = (uri: string) => {
    if (uri === emptyStarUri) {
      setIsFavourite(true);
      uri = filledStarUri;
    } else {
      uri = emptyStarUri;
      setIsFavourite(false);
    }
    setStarUri(uri);
  };

  const handleNameEditing = (text: string) => {
    const isValid = text.trim().length > 0;
    setName(text);
    setIsValidName(isValid);
  };
  const handlePhoneNumberChange = (text: string) => {
    setPhoneNumber(text);
    setPhoneNumberValid(text.length === 10);
  };

  const handleLandlineChange = (text: string) => {
    setLandlineNumber(text);
    setLandlineNumberValid(text.length >= 8);
  };

  const handleImagePress = () => {
    Alert.alert(
      'Select Image',
      'Choose an option to select an image',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Select from File',
          onPress: () => {
            ImagePicker();
          },
        },
        {
          text: 'Select from Camera',
          onPress: () => {
            openCamera();
          },
        },
      ],
      {cancelable: true},
    );
  };
  const ImagePicker = () => {
    const storageOptions = {
      path: 'image',
    };
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      quality: 1,
      selectionLimit: 1,
      ...storageOptions,
    };

    launchImageLibrary(options, response => {
      const selectedUri = response.assets?.[0]?.uri ?? emptyImageUri;
      setSelectedImage(selectedUri);
    });
  };

  const openCamera = () => {
    const storageOptions = {
      path: 'image',
    };
    const options: CameraOptions = {
      mediaType: 'photo',
      ...storageOptions,
    };

    launchCamera(options, response => {
      const selectedUri = response.assets?.[0]?.uri ?? emptyImageUri;
      setSelectedImage(selectedUri);
    });
  };

  const handleDelete = () => {
    Alert.alert(
      'Confirm Deletion',
      'Are you sure you want to delete this contact?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            deleteContact();
          },
        },
      ],
      {cancelable: false},
    );
  };

  const deleteContact = () => {
    db.transaction(txn => {
      txn.executeSql(
        'DELETE FROM Contacts WHERE uid = ?',
        [route.params.contact.uid],
        (tx: any, res: {rowsAffected: number}) => {
          if (res.rowsAffected > 0) {
            if (isFromFavourite) navigation.goBack();
            else navigation.popToTop();
          }
        },
      );
    });
  };

  const handleSave = () => {
    if (route.params && route.params.contact && route.params.contact.uid) {
      contact.uid = route.params.contact.uid;
      updateContact();
    } else {
      AddNewContact();
    }
  };

  const AddNewContact = () => {
    if (
      phoneNumberValid &&
      landlineNumberValid &&
      isValidName &&
      phoneNumber.length != 0 &&
      landlineNumber.length != 0 &&
      name.length != 0
    ) {
      db.transaction(txn => {
        txn.executeSql(
          'INSERT INTO Contacts (name, phoneNumber, imageUri, isFavourite, landlineNumber) VALUES (?, ?, ?, ?, ?)',
          [name, phoneNumber, selectedImage, isFavourite, landlineNumber],
          (txt: any, res: {rowsAffected: number}) => {
            if (res.rowsAffected > 0) {
              navigation.popToTop();
            } else {
              console.log('Failed to insert data.');
            }
          },
        );
      });
    }
  };

  const updateContact = () => {
    if (phoneNumberValid && landlineNumberValid && isValidName) {
      db.transaction(tx => {
        tx.executeSql(
          'UPDATE Contacts SET name = ?, phoneNumber = ?, imageUri = ?, isFavourite = ?, landlineNumber = ? WHERE uid = ?',
          [
            name,
            phoneNumber,
            selectedImage,
            isFavourite,
            landlineNumber,
            contact.uid,
          ],
          (tx: any, res: any) => {
            if (isFromFavourite) navigation.goBack();
            else navigation.popToTop();
          },
        );
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          onPress={() => {
            isFromFavourite
              ? navigation.navigate('FavouriteList')
              : navigation.navigate('ContactList');
          }}>
          <Image source={backIconUri} style={styles.icons} />
        </TouchableOpacity>
        <Text style={styles.headerText}>
          {route.params && route.params.contact.uid
            ? 'Update Contact'
            : 'Add New Contact'}
        </Text>
        <TouchableOpacity onPress={() => changeUri(starUri)}>
          <Image source={starUri} style={styles.icons} />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.imageContainer}
        onPress={handleImagePress}>
        <Image source={{uri: selectedImage}} style={styles.circularImage} />

        <Text style={styles.imageText}>
          {' '}
          {route.params && route.params.contact.uid
            ? 'Tap to Update Photo'
            : 'Tap to Add Photo'}
        </Text>
      </TouchableOpacity>
      <View style={styles.inputContainer}>
        {!isValidName && (
          <Text style={styles.errorMessage}>Name field is mandatory!</Text>
        )}
        <TextInput
          style={[styles.input, !isValidName && styles.inputError]}
          placeholder="Name"
          value={name}
          onChangeText={handleNameEditing}
        />
        {!phoneNumberValid && (
          <Text style={styles.errorMessage}>
            Phone number must be 10 digits
          </Text>
        )}
        <TextInput
          style={[styles.input, !phoneNumberValid && styles.inputError]}
          placeholder="Phone Number"
          keyboardType="phone-pad"
          maxLength={10}
          value={phoneNumber}
          onChangeText={handlePhoneNumberChange}
          onBlur={() => handlePhoneNumberChange(phoneNumber)}
        />
        {!landlineNumberValid && (
          <Text style={styles.errorMessage}>
            Landline number must between 8-10 digits
          </Text>
        )}
        <TextInput
          style={[styles.input, !landlineNumberValid && styles.inputError]}
          placeholder="Landline"
          keyboardType="phone-pad"
          maxLength={10}
          value={landlineNumber}
          onChangeText={handleLandlineChange}
          onBlur={() => handleLandlineChange(landlineNumber)}
        />
      </View>
      <TouchableOpacity style={styles.saveButton} onPress={handleSave}>
        <Text style={styles.saveButtonText}>
          {route.params && route.params.contact.uid ? 'Update' : 'Save'}
        </Text>
      </TouchableOpacity>
      {route.params && route.params.contact.uid && (
        <TouchableOpacity style={styles.deleteButton} onPress={handleDelete}>
          <Text style={styles.saveButtonText}>Delete</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  circularImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderColor: 'lightgray',
    borderWidth: 1,
    marginBottom: 10,
  },
  imageText: {
    fontSize: 16,
    color: 'gray',
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
  },
  inputError: {
    borderColor: 'red',
  },
  errorMessage: {
    color: 'red',
    margin: 0,
    padding: 0,
  },
  saveButton: {
    backgroundColor: 'blue',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  saveButtonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  deleteButton: {
    backgroundColor: 'red',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 10,
  },
  icons: {
    width: 30,
    height: 30,
  },
});

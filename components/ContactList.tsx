import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../App';

const editIcon = require('../images/edit.png');
const deleteIcon = require('../images/delete.png');

import {DrawerActions, useIsFocused} from '@react-navigation/native';
import {Swipeable} from 'react-native-gesture-handler';
import {openDatabase} from 'react-native-sqlite-storage';

var db = openDatabase({name: 'ContactDatabases.db'});

type ContactListProps = NativeStackScreenProps<
  RootStackParamList,
  'ContactList'
>;

export default function ContactList({navigation}: ContactListProps) {
  const menuIcon = require('../images/menu.png');
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredContacts, setFilteredContacts] = useState<Contact[]>([]);
  const [contactlist, setContactlist] = useState<Contact[]>([]);
  const isFocused = useIsFocused();

  useEffect(() => {
    db.transaction(txn => {
      txn.executeSql(
        'SELECT * FROM Contacts ORDER BY LOWER(name) ASC',
        [],
        (txn: any, res: any) => {
          var temp: Contact[] = [];
          for (let i = 0; i < res.rows.length; ++i) {
            temp.push(res.rows.item(i));
          }
          setContactlist(temp);
        },
      );
    });
  }, [isFocused]);

  const navigateToUpdateContact = (uid: any) => {
    const contact = contactlist.find(contact => contact.uid === uid);
    if (contact) {
      navigation.navigate('AddNewContact', {
        contact: contact,
        isFromFavourite: false,
      });
    }
  };

  const contact: Contact = {
    name: '',
    phoneNumber: '',
    imageUri: '',
    isFavourite: false,
    landlineNumber: '',
  };

  const openMenu = () => {
    navigation.dispatch(DrawerActions.openDrawer);
  };

  const handleSearch = (query: any) => {
    setSearchQuery(query);
    filterContacts(query);
  };

  const filterContacts = (query: any) => {
    const filtered = contactlist.filter(contact =>
      contact.name.toLowerCase().includes(query.toLowerCase()),
    );
    setFilteredContacts(filtered);
  };

  const handleDelete = (uid: any) => {
    db.transaction(txn => {
      txn.executeSql(
        'DELETE FROM Contacts WHERE uid = ?',
        [uid],
        (txn: any, res: any) => {
          if (res.rowsAffected > 0) {
            const updatedContactList = contactlist.filter(
              contact => contact.uid !== uid,
            );
            setContactlist(updatedContactList);
          } else {
            console.log('Unable to delete contact with uid : ', uid);
          }
        },
      );
    });
  };

  const renderItem = ({item}) => {
    const renderRightActions = () => (
      <View style={styles.rightActionsContainer}>
        <TouchableOpacity
          style={[styles.actionButton, styles.editButton]}
          onPress={() => navigateToUpdateContact(item.uid)}>
          <Image source={editIcon} style={styles.icons} />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.actionButton, styles.deleteButton]}
          onPress={() => handleDelete(item.uid)}>
          <Image source={deleteIcon} style={styles.icons} />
        </TouchableOpacity>
      </View>
    );

    return (
      <Swipeable
        renderRightActions={renderRightActions}
        overshootRight={false}
        containerStyle={styles.swipeableContainer}>
        <TouchableOpacity
          key={item.uid}
          onPress={() => navigateToUpdateContact(item.uid)}>
          <View style={styles.userCard}>
            <Image source={{uri: item.imageUri}} style={styles.userImage} />
            <Text style={styles.userName}>{item.name}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={openMenu}>
          <Image
            source={menuIcon}
            width={30}
            height={30}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>
        <Text style={styles.headingText}>Contact List</Text>
      </View>
      <View style={styles.searchBarContainer}>
        <TextInput
          style={styles.searchInput}
          placeholder="Search by name"
          value={searchQuery}
          onChangeText={handleSearch}
        />
      </View>
      <FlatList
        data={searchQuery === '' ? contactlist : filteredContacts}
        renderItem={renderItem}
        keyExtractor={item => item.uid.toString()}
        ListEmptyComponent={
          <Text style={styles.emptyText}>
            {searchQuery === ''
              ? 'No contacts added yet! Please add first to show up.'
              : 'No matching contacts found.'}
          </Text>
        }
      />
      <TouchableOpacity
        style={styles.addButton}
        onPress={() => {
          navigation.navigate('AddNewContact', {
            contact: contact,
            isFromFavourite: false,
          });
        }}>
        <Text style={styles.addButtonText}>➕</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  headingText: {
    fontSize: 28,
    color: '#000000',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
    marginLeft: 30,
  },
  scrollView: {},
  userCard: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 3,
    padding: 8,
    borderRadius: 10,
  },
  userImage: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginRight: 14,
  },
  userName: {
    fontSize: 16,
    fontWeight: '600',
  },
  emptyText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    fontWeight: '500',
    marginTop: 300,
    marginHorizontal: 20,
  },
  addButton: {
    position: 'absolute',
    bottom: 16,
    right: 16,
    backgroundColor: '#F5C469',
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButtonText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  searchBarContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderWidth: 0.5,
    borderColor: 'black',
    borderRadius: 10,
    marginHorizontal: 10,
  },
  searchInput: {
    backgroundColor: '#F0F0F0',
    padding: 10,
    borderRadius: 5,
  },
  swipeableContainer: {
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: '#E0E0E0',
  },
  rightActionsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 75,
    height: '100%',
  },
  editButton: {
    backgroundColor: '#4CAF50', // Green color for edit button
  },
  deleteButton: {
    backgroundColor: '#F44336', // Red color for delete button
  },
  icons: {
    width: 24,
    height: 24,
  },
});

import React, {useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {DrawerActions, useIsFocused} from '@react-navigation/native';
//navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../App';

import {openDatabase} from 'react-native-sqlite-storage';

var db = openDatabase({name: 'ContactDatabases.db'});

type ContactListProps = NativeStackScreenProps<
  RootStackParamList,
  'ContactList'
>;

const menuIcon = require('../images/menu.png');

export default function FavouriteList({navigation}: ContactListProps) {
  const [favouriteList, setFavouriteList] = useState<Contact[]>([]);
  const isFocused = useIsFocused();
  useEffect(() => {
    db.transaction(txn => {
      txn.executeSql(
        'SELECT * FROM Contacts ORDER BY name ASC',
        [],
        (txn: any, res: any) => {
          var fav: Contact[] = [];
          for (let i = 0; i < res.rows.length; ++i) {
            if (res.rows.item(i).isFavourite === 1) fav.push(res.rows.item(i));
          }
          setFavouriteList(fav);
        },
      );
    });
  }, [isFocused]);

  const navigateToUpdateContact = (uid: any) => {
    const contact = favouriteList.find(contact => contact.uid === uid);
    if (contact) {
      navigation.navigate('AddNewContact', {
        contact: contact,
        isFromFavourite: true,
      });
    }
  };
  const openMenu = () => {
    navigation.dispatch(DrawerActions.openDrawer);
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={openMenu}>
          <Image
            source={menuIcon}
            width={30}
            height={30}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>
        <Text style={styles.headingText}>Favourite List</Text>
      </View>
      <ScrollView style={styles.scrollView}>
        {favouriteList.length === 0 ? (
          <Text style={styles.emptyText}>
            No Favourite contacts added yet ! Please mark favourite first to
            show up.
          </Text>
        ) : (
          favouriteList.map(({uid, name, imageUri}) => (
            <TouchableOpacity
              key={uid}
              onPress={() => navigateToUpdateContact(uid)}>
              <View style={styles.userCard}>
                <Image source={{uri: imageUri}} style={styles.userImage} />
                <View>
                  <Text style={styles.userName}>{name}</Text>
                </View>
              </View>
            </TouchableOpacity>
          ))
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  headingText: {
    fontSize: 28,
    color: '#000000',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
    marginLeft: 30,
  },
  scrollView: {},
  userCard: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 3,
    padding: 8,
    borderRadius: 10,
  },
  emptyText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 26,
    fontWeight: '500',
    marginTop: 300,
    marginHorizontal: 20,
  },
  userImage: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginRight: 14,
  },
  userName: {
    fontSize: 16,
    fontWeight: '600',
  },
});

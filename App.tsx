import React from 'react';
import {StyleSheet} from 'react-native';
import 'react-native-gesture-handler';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import AddNewContact from './components/AddNewContact';
import ContactList from './components/ContactList';

import {createDrawerNavigator} from '@react-navigation/drawer';
import FavouriteList from './components/FavouriteList';

const Drawer = createDrawerNavigator<RootDrawerParamList>();

export type RootStackParamList = {
  ContactList: undefined;
  AddNewContact: {contact: Contact; isFromFavourite: false};
  FavouriteList: undefined;
};

export type RootDrawerParamList = {
  DrawerContactList: undefined;
  FavouriteList: undefined;
};

const stack = createNativeStackNavigator<RootStackParamList>();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="DrawerContactList">
        <Drawer.Screen name="DrawerContactList" component={StackNavigator} />
        <Drawer.Screen name="FavouriteList" component={FavouriteList} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const StackNavigator = () => {
  return (
    <stack.Navigator initialRouteName="ContactList">
      <stack.Screen
        name="ContactList"
        component={ContactList}
        options={{
          headerShown: false,
        }}
      />
      <stack.Screen
        name="AddNewContact"
        component={AddNewContact}
        options={{headerShown: false}}
      />
      <stack.Screen
        name="FavouriteList"
        component={FavouriteList}
        options={{headerShown: false}}
      />
    </stack.Navigator>
  );
};

const styles = StyleSheet.create({});
